mod commands;

use std::{collections::{HashMap, HashSet}, env, sync::Arc};

use serenity::{async_trait, client::bridge::gateway::ShardManager, framework::{
        StandardFramework,
        standard::macros::group,
    }, http::Http, model::id::ChannelId, model::id::GuildId, model::{guild::Member, event::ResumedEvent, gateway::Ready, id::MessageId}, prelude::*};

use tracing::{error, info};
use tracing_subscriber::{
    FmtSubscriber,
    EnvFilter
};

use chrono::{DateTime, Local};

use tokio::sync::RwLock;

pub const LOG_ENV: &'static str = "QBOT_LOG";

use commands::admin::*;

pub struct ShardManagerContainer;

impl TypeMapKey for ShardManagerContainer {

    type Value = Arc<Mutex<ShardManager>>;
}

struct LogChannelMap;

impl TypeMapKey for LogChannelMap {
    type Value = Arc<RwLock<HashMap<GuildId, ChannelId>>>;
}

struct Handler;

#[async_trait]
impl EventHandler for Handler {

    async fn ready(&self, _: Context, ready: Ready) {
        info!("Connected as {}", ready.user.name);
    }

    async fn resume(&self, _: Context, _: ResumedEvent) {
        info!("resumed");
    }

    async fn guild_member_addition(&self, ctx: Context, guild_id: GuildId, new_member: Member) {

         let log_channel_lock = {
                let data_read = ctx.data.read().await;
                data_read.get::<LogChannelMap>().expect("Expected LogChannelMap in TypeMap").clone()
         };

        let channel: ChannelId;

        {
            let log_channel = log_channel_lock.read().await;
            channel = log_channel.get(&guild_id).unwrap().clone();
        }

        let embed = channel.send_message(&ctx.http, |m| {

            m.embed(|e| {

                e.description(&new_member.mention().to_string());

                e.author(|a| {
                    a.name("MEMBER JOIN");
                    a.icon_url(format!("{}", &new_member.user.avatar_url().unwrap()));

                    a
                });

                e.footer(|f| {
                    f.text(format!("{}", Local::now()));
                    f
                });

                e
            });

            m
        });

        embed.await
            .expect("Failed to send embed");
    }

    async fn message_delete(&self, ctx: Context, channel_id: ChannelId, deleted_message_id: MessageId, guild_id: Option<GuildId>) {

        if let Some(gid) = guild_id {

            let mesg = ctx.cache.message(channel_id, deleted_message_id).await;

            let log_channel_lock = {
                let data_read = ctx.data.read().await;
                data_read.get::<LogChannelMap>().expect("Expected LogChannelMap in TypeMap").clone()
            };

            let channel: ChannelId;

            {
                let log_channel = log_channel_lock.read().await;
                channel = log_channel.get(&gid).unwrap().clone();
                println!("{:?}", channel);
            }

            let msg = mesg.unwrap();

            let embed = channel.send_message(&ctx.http, |m| {
                m.embed(|e| {
                    e.description(&msg.content);

                    e.author(|a| {
                        a.name("MESSAGE DELETED");
                        a.icon_url(&msg.author.avatar_url().unwrap());
                        a
                    });

                    e.fields(vec![
                        ("author", format!("{}", &msg.author.mention().to_string()), true),
                        ("channel", format!("{}", &msg.channel_id.mention().to_string()), true),
                        ("issued", format!("{}", &msg.timestamp), false)
                    ]);

                    e.footer(|f| {
                        f.text(format!("{}", Local::now()));
                        f
                    });
                    e
                });

                m
            });

            embed.await
                .expect("Failed to send embed.");
        }
    }
}

#[group]
#[commands(embed)]
#[commands(embedpr)]
#[commands(setlogchannel)]
struct General;

#[tokio::main]
async fn main() {

    dotenv::dotenv()
        .expect("Failed to load `.env` file.");

    let subscriber = FmtSubscriber::builder()
        .with_env_filter(EnvFilter::from_env(&LOG_ENV))
        .finish();

    tracing::subscriber::set_global_default(subscriber)
        .expect("Failed to start the logger.");

    let token = env::var("DISCORD_TOKEN")
        .expect("Expected a token in the environment.");

    let http = Http::new_with_token(&token);

    let (owners, _bot_id) = match http.get_current_application_info().await {

        Ok(info) => {
            let mut owners = HashSet::new();
            owners.insert(info.owner.id);

            (owners, info.id)
        }
        Err(why) => panic!("Could not access application info {:?}", why),
    };

    let framework = StandardFramework::new()
        .configure(|c| c
                   .owners(owners)
                   .prefix("~"))
        .group(&GENERAL_GROUP);

    let mut client = Client::builder(&token)
        .framework(framework)
        .event_handler(Handler)
        .await
        .expect("Err creating client.");

    client.cache_and_http.cache.set_max_messages(128).await;

    {
        let mut data = client.data.write().await;
        data.insert::<ShardManagerContainer>(client.shard_manager.clone());
        data.insert::<LogChannelMap>(Arc::new(RwLock::new(HashMap::new())));
    }

    let shard_manager = client.shard_manager.clone();

    tokio::spawn(async move {

        tokio::signal::ctrl_c().await
            .expect("Could not register `ctrl+c` handler");

        info!("Gracefully shutting down.");
        shard_manager.lock().await.shutdown_all().await;

    });

    if let Err(why) = client.start().await {

        error!("Client error: {:?}", why);
    }
}
