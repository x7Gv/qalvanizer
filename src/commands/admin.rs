use serenity::prelude::*;
use serenity::model::prelude::*;
use serenity::framework::standard::{
    Args, CommandResult,
    macros::command,
};

use crate::LogChannelMap;

#[command]
pub async fn setlogchannel(ctx: &Context, msg: &Message, mut args: Args) -> CommandResult {

    let gid = match msg.guild_id {
        Some(it) => it,
        _ => return Ok(()),
    };

    let channels = gid.channels(&ctx).await?;

    let arg = args.single_quoted::<u64>();
    let id = match arg {
        Ok(id) => { id },
        Err(_) => {
            msg.channel_id.say(&ctx, "[!] expected a `ChannelId` to be inserted as an argument.").await?;
            return Ok(())
        }
    };

    let channel = ChannelId(id);

    if !channels.contains_key(&channel) {

        msg.channel_id.say(&ctx, format!("failed to validate **ChannelId** :: `{}`", id))
            .await?;

        return Ok(())
    }

    let log_channel_lock = {
        let data_read = ctx.data.read().await;
        data_read.get::<LogChannelMap>().expect("Expected LogChannelMap in TypeMap").clone()
    };

    {
        let mut log_channels = log_channel_lock.write().await;
        log_channels.insert(gid, channel);
    }

    msg.reply(&ctx, "✅ LogChannelMap entry added").await?;

    Ok(())
}
