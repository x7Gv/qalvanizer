use serenity::{builder::CreateEmbedFooter, prelude::*};
use serenity::model::prelude::*;
use serenity::framework::standard::{
    Args, CommandResult,
    macros::command,
};

#[command]
pub async fn embed(ctx: &Context, msg: &Message, mut args: Args) -> CommandResult {

    let arg1 = match args.single_quoted::<String>() {
        Ok(v) => v,
        Err(e) => {
            msg.channel_id.say(&ctx.http, "Missing arg 1").await?;
            panic!(e);
        },
    };

    let arg2 = match args.single_quoted::<String>() {
        Ok(v) => v,
        Err(e) => {
            msg.channel_id.say(&ctx.http, "Missing arg 2").await?;
            panic!(e);
        },
    };

    msg.channel_id.say(&ctx.http, format!("da oneth argumentah given {}", &arg1)).await?;
    msg.channel_id.say(&ctx.http, format!("da twoth argumentah given {}", &arg2)).await?;

    msg.channel_id.send_message(&ctx.http, |m| {

        m.content("jou");

        m.embed(|e| {
            e.title("Haloust");
            e.fields(vec![
                ("arg1", arg1, false),
                ("arg2", arg2, false),
            ]);

            e.set_footer(CreateEmbedFooter::default().text("nih").to_owned());

            e
        });

        m
    }).await?;

    Ok(())
}

#[command]
pub async fn embedpr(ctx: &Context, msg: &Message, mut args: Args) -> CommandResult {

    let mut quoted_args = Vec::<String>::new();

    for arg in args.iter::<String>() {

        match arg {
            Ok(a) => { quoted_args.push(a); }
            Err(_) => {}
        }
    }

    for item in quoted_args {
        msg.channel_id.say(&ctx, item).await?;
    }

    Ok(())
}
